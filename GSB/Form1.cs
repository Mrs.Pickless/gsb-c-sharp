﻿using GSB.Dal;
using GSB.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB
{
    public partial class Form1 : Form
    {
        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            GsbDao gsbDao = new GsbDao();
            dataGridView1.DataSource = gsbDao.getFiche();

            ManageDate test = new ManageDate();
            Controller.Manager test2 = new Controller.Manager();
            test2.checkDateTime();
        }
    }
}
